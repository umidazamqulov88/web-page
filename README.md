# Library Catalog RESTful API

## Entities

1. **Books**
   - Attributes: ISBN (string, unique), Title (string), AuthorID (string), Genre (string), PublicationYear (integer), Availability (boolean)

2. **Authors**
   - Attributes: AuthorID (string, unique), Name (string), Biography (text)

3. **Categories**
   - Attributes: CategoryID (string, unique), Name (string), Description (text)

## Operations

### Books

- **List all books**
  - Endpoint: `GET /books`
  - Description: Retrieve a list of all books in the library.
  - Parameters: None
  - Response: List of books

- **Get book details**
  - Endpoint: `GET /books/{ISBN}`
  - Description: Retrieve detailed information about a specific book.
  - Parameters: ISBN (Unique identifier)
  - Response: Book details

- **Add a new book**
  - Endpoint: `POST /books`
  - Description: Add a new book to the library catalog.
  - Parameters: Book details in the request body
  - Response: Newly added book details

- **Update book details**
  - Endpoint: `PUT /books/{ISBN}`
  - Description: Update information about a specific book.
  - Parameters: ISBN (Unique identifier), Updated book details in the request body
  - Response: Updated book details

- **Delete a book**
  - Endpoint: `DELETE /books/{ISBN}`
  - Description: Remove a book from the library catalog.
  - Parameters: ISBN (Unique identifier)
  - Response: Success message

### Authors

- **List all authors**
  - Endpoint: `GET /authors`
  - Description: Retrieve a list of all authors in the library.
  - Parameters: None
  - Response: List of authors

- **Get author details**
  - Endpoint: `GET /authors/{AuthorID}`
  - Description: Retrieve detailed information about a specific author.
  - Parameters: AuthorID (Unique identifier)
  - Response: Author details

- **Add a new author**
  - Endpoint: `POST /authors`
  - Description: Add a new author to the library catalog.
  - Parameters: Author details in the request body
  - Response: Newly added author details

- **Update author details**
  - Endpoint: `PUT /authors/{AuthorID}`
  - Description: Update information about a specific author.
  - Parameters: AuthorID (Unique identifier), Updated author details in the request body
  - Response: Updated author details

- **Delete an author**
  - Endpoint: `DELETE /authors/{AuthorID}`
  - Description: Remove an author from the library catalog.
  - Parameters: AuthorID (Unique identifier)
  - Response: Success message

### Categories

- **List all categories**
  - Endpoint: `GET /categories`
  - Description: Retrieve a list of all book categories in the library.
  - Parameters: None
  - Response: List of categories

- **Get category details**
  - Endpoint: `GET /categories/{CategoryID}`
  - Description: Retrieve detailed information about a specific book category.
  - Parameters: CategoryID (Unique identifier)
  - Response: Category details

- **Add a new category**
  - Endpoint: `POST /categories`
  - Description: Add a new book category to the library catalog.
  - Parameters: Category details in the request body
  - Response: Newly added category details

- **Update category details**
  - Endpoint: `PUT /categories/{CategoryID}`
  - Description: Update information about a specific book category.
  - Parameters: CategoryID (Unique identifier), Updated category details in the request body
  - Response: Updated category details

- **Delete a category**
  - Endpoint: `DELETE /categories/{CategoryID}`
  - Description: Remove a book category from the library catalog.
  - Parameters: CategoryID (Unique identifier)
  - Response: Success message

## Functional Requirements

1. Users should be able to view a list of all books, authors, and categories.
2. Users should be able to retrieve detailed information about a specific book, author, or category.
3. Users should be able to add, update, and delete books, authors, and categories.
4. Books should have a status indicating their availability (e.g., available, checked out).

## Non-functional Requirements (Richardson Maturity Model)

### Level 1 - Resources

- **Identification:** Each resource (books, authors, categories) should be uniquely identified using URIs.

### Level 2 - HTTP Verbs

- **Standard HTTP Methods:** Use standard HTTP methods (GET, POST, PUT, DELETE) for CRUD operations.

### Level 3 - Hypermedia Controls

- **Hypermedia Links:** Include hypermedia links in responses to guide clients to related resources.

## REST API

### Collections

- `/books`
- `/authors`
- `/categories`

### Filters

- Filtering books by genre: `/books?genre=mystery`
- Filtering authors by name: `/authors?name=John Doe`
- Filtering categories by name: `/categories?name=fiction`

### Pagination

- Paginate the list of books: `/books?page=1&limit=10`
- Paginate the list of authors: `/authors?page=2&limit=5`
- Paginate the list of categories: `/categories?page=3&limit=8`

### Errors and Authentication

- Appropriate HTTP status codes for responses
- Implement authentication using API keys or tokens.

### JWT Authentication

- **Authentication Endpoint**: `/login`
  - **Method**: `POST`
  - **Description**: User login to obtain a JWT token with a valid email and password.
  - **Request Body**:
    ```json
    {
      "email": "user@example.com",
      "password": "password"
    }
    ```
  - **Response**:
    ```json
    {
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."
    }
    ```
  - **Status Codes**:
    - `200`: Successful login
    - `401`: Unauthorized (Invalid credentials)

- **JWT-Protected Endpoints**:
  For the following endpoints, include the obtained JWT token in the `Authorization` header of your request.
  - `/books`
  - `/books/{ISBN}`
  - `/authors`
  - `/authors/{AuthorID}`
  - `/categories`
  - `/categories/{CategoryID}`

- **Example Request with JWT Token**:
  ```http
  GET /books
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...
